﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace UdemyDotNet.API.Migrations
{
    /// <inheritdoc />
    public partial class SeedingDataForDifficultiesAndRegions : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Difficulties",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { new Guid("0b9c7cda-d372-4d8d-b4e9-d38cfd116bb0"), "Medium" },
                    { new Guid("23344aa8-dbc8-4aa9-b694-d4d8051072a6"), "Easy" },
                    { new Guid("dcfa98aa-ff83-4b5f-839c-0d99d0e44829"), "Hard" }
                });

            migrationBuilder.InsertData(
                table: "Regions",
                columns: new[] { "Id", "Code", "Name", "RegionImageUrl" },
                values: new object[,]
                {
                    { new Guid("49dcbb62-3789-49d6-91bd-09ea646c8f5b"), "NSN", "Nelson", "imageurl.jpg" },
                    { new Guid("86ed04e8-8bf1-4d75-be13-6bfe0905b5ab"), "AKL", "Auckland", "imageurl.jpg" },
                    { new Guid("9af4b6ac-afb8-4ff1-acd0-2f6fadd1ca84"), "STL", "Southland", "imageurl.jpg" },
                    { new Guid("ff4154e2-7ad7-4df4-89bc-024560697f3e"), "WGN", "Wellington", "imageurl.jpg" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Difficulties",
                keyColumn: "Id",
                keyValue: new Guid("0b9c7cda-d372-4d8d-b4e9-d38cfd116bb0"));

            migrationBuilder.DeleteData(
                table: "Difficulties",
                keyColumn: "Id",
                keyValue: new Guid("23344aa8-dbc8-4aa9-b694-d4d8051072a6"));

            migrationBuilder.DeleteData(
                table: "Difficulties",
                keyColumn: "Id",
                keyValue: new Guid("dcfa98aa-ff83-4b5f-839c-0d99d0e44829"));

            migrationBuilder.DeleteData(
                table: "Regions",
                keyColumn: "Id",
                keyValue: new Guid("49dcbb62-3789-49d6-91bd-09ea646c8f5b"));

            migrationBuilder.DeleteData(
                table: "Regions",
                keyColumn: "Id",
                keyValue: new Guid("86ed04e8-8bf1-4d75-be13-6bfe0905b5ab"));

            migrationBuilder.DeleteData(
                table: "Regions",
                keyColumn: "Id",
                keyValue: new Guid("9af4b6ac-afb8-4ff1-acd0-2f6fadd1ca84"));

            migrationBuilder.DeleteData(
                table: "Regions",
                keyColumn: "Id",
                keyValue: new Guid("ff4154e2-7ad7-4df4-89bc-024560697f3e"));
        }
    }
}
