﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UdemyDotNet.API.CustomActionFilters;
using UdemyDotNet.API.Models.Domain;
using UdemyDotNet.API.Models.DTO;
using UdemyDotNet.API.Repositories;

namespace UdemyDotNet.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WalksController : ControllerBase
    {
        public readonly IMapper mapper;
        private readonly IWalkRepository walkRepository;

        public WalksController(IMapper mapper, IWalkRepository walkRepository)
        {
            this.mapper = mapper;
            this.walkRepository = walkRepository;
        }

        // GET ALL WALKS
        // GET: https://localhost:44375/api/walks
        [HttpGet]
        public async Task<IActionResult> GetAll(
            [FromQuery] string? filterOn, [FromQuery] string? filterQuery, 
            [FromQuery] string? sortBy, [FromQuery] bool? isAcending,
            [FromQuery] int pageNumber = 1, [FromQuery] int pageSize = 100)
        {
            // Get Data from Database - Domain models
            var walksDomain = await walkRepository.GetAllAsync(
                filterOn, filterQuery, 
                sortBy, isAcending ?? true, 
                pageNumber, pageSize);

            // Return DTOs
            return Ok(mapper.Map<List<WalkDto>>(walksDomain));
        }

        // GET SINGLE WALK (Get Walk by ID)
        // GET: https://localhost:44375/api/walk/{id}
        [HttpGet]
        [Route("{id:Guid}")]
        public async Task<IActionResult> GetById([FromRoute] Guid id)
        {
            // Get Data from Database - Domain models
            var walkDomain = await walkRepository.GetByIdAsync(id);

            // Check if walk exists
            if (walkDomain == null)
                return NotFound();

            var walkDto = mapper.Map<WalkDto>(walkDomain);

            // Return DTO
            return Ok(walkDto);
        }

        // CREATE WALK
        // POST:https://localhost:44375/api/walks
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Create([FromBody] AddWalkDto addWalkDto)
        {
            // Map Dto to domain model
            var walkDomainModel = mapper.Map<Walk>(addWalkDto);

            await walkRepository.CreateAsync(walkDomainModel);

            //Map domain model to DTO
            var walkDto = mapper.Map<WalkDto>(walkDomainModel);

            return Ok(walkDto);
        }

        // PUT SINGLE WALK (Update Walk by ID)
        // PUT: https://localhost:44375/api/walk/{id}
        [HttpPut]
        [Route("{id:Guid}")]
        [ValidateModel]
        public async Task<IActionResult> Update([FromRoute] Guid id, UpdateWalkDto updateWalkDto)
        {
            // Map or Convert DTO to Domain Model
            var walkDomain = mapper.Map<Walk>(updateWalkDto);

            walkDomain = await walkRepository.UpdateAsync(id, walkDomain);

            // Check if walk exists
            if (walkDomain == null)
                return NotFound();

            // Return DTO
            return Ok(mapper.Map<WalkDto>(walkDomain));
        }

        // DELETE SINGLE REGION (Remove Region by ID)
        // PUT: https://localhost:44375/api/walks/{id}
        [HttpDelete]
        [Route("{id:Guid}")]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            var walkDomain = await walkRepository.DeleteAsync(id);

            if (walkDomain == null) 
                return NotFound();

            var walkDto = mapper.Map<WalkDto>(walkDomain);
            return Ok(walkDto);
        }
    }
}
