﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UdemyDotNet.API.Data;
using UdemyDotNet.API.Models.Domain;
using UdemyDotNet.API.Models.DTO;
using UdemyDotNet.API.Repositories;

namespace UdemyDotNet.API.Controllers
{
    // https://localhost:44375/api/regions
    [Route("api/[controller]")]
    [ApiController]
    public class RegionsController : ControllerBase
    {
        private readonly UdemyDotNetDbContext dbContext;
        private readonly IRegionRepository regionRepository;
        private readonly IMapper mapper;

        public RegionsController(UdemyDotNetDbContext dbContext, IRegionRepository regionRepository, IMapper mapper)
        {
            this.dbContext = dbContext;
            this.regionRepository = regionRepository;
            this.mapper = mapper;
        }

        // GET ALL REGIONS
        // GET: https://localhost:44375/api/regions
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            // Get Data from Database - Domain models
            var regionsDomain = await regionRepository.GetAllAsync();

            // Map Domain models to DTOs
            //var regionsDto = new List<RegionDto>();
            //foreach (var regionDomain in regionsDomain)
            //{
            //    regionsDto.Add(new RegionDto()
            //    {
            //        Id = regionDomain.Id,
            //        Name = regionDomain.Name,
            //        Code = regionDomain.Code,
            //        RegionImageUrl = regionDomain.RegionImageUrl,
            //    });
            //}

            var regionsDto = mapper.Map<List<RegionDto>>(regionsDomain);

            // Return DTOs
            return Ok(regionsDto);
        }

        // GET SINGLE REGION (Get Region by ID)
        // GET: https://localhost:44375/api/regions/{id}
        [HttpGet]
        [Route("{id:Guid}")]
        public async Task<IActionResult> GetById([FromRoute] Guid id)
        {
            // Get Data from Database - Domain models
            var regionDomain = await regionRepository.GetByIdAsync(id);

            // Check if region exists
            if (regionDomain == null)
                return NotFound();

            // Map Domain model to DTO
            //var regionDto = new RegionDto
            //{
            //    Id = regionDomain.Id,
            //    Name = regionDomain.Name,
            //    Code = regionDomain.Code,
            //    RegionImageUrl = regionDomain.RegionImageUrl,
            //};

            var regionDto = mapper.Map<RegionDto>(regionDomain);

            // Return DTO
            return Ok(regionDto);
        }

        // POST SINGLE REGION (Create Region)
        // POST: https://localhost:44375/api/regions
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] AddRegionDto addRegionDto)
        {
            // Map or Convert DTO to Domain Model
            //var regionDomainModel = new Region
            //{
            //    Name = addRegionDto.Name,
            //    Code = addRegionDto.Code,
            //    RegionImageUrl = addRegionDto.RegionImageUrl,
            //};

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var regionDomainModel = mapper.Map<Region>(addRegionDto);

            // Use Domain Model to create Region
            regionDomainModel = await regionRepository.CreateAsync(regionDomainModel);

            // Map Domain model back to DTO
            //var regionDto = new RegionDto
            //{
            //    Id = regionDomainModel.Id,
            //    Name = regionDomainModel.Name,
            //    Code = regionDomainModel.Code,
            //    RegionImageUrl = regionDomainModel.RegionImageUrl,
            //};

            var regionDto = mapper.Map<RegionDto>(regionDomainModel);

            // Return DTO
            return CreatedAtAction(nameof(GetById), new { id = regionDto.Id }, regionDto);
        }

        // PUT SINGLE REGION (Update Region by ID)
        // PUT: https://localhost:44375/api/regions/{id}
        [HttpPut]
        [Route("{id:Guid}")]
        public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] UpdateRegionDto updateRegionDto)
        {
            // Map or Convert DTO to Domain Model
            //var regionDomainModel = new Region
            //{
            //    Name = updateRegionDto.Name,
            //    Code = updateRegionDto.Code,
            //    RegionImageUrl = updateRegionDto.RegionImageUrl,
            //};

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var regionDomainModel = mapper.Map<Region>(updateRegionDto);

            // Use Domain Model to update Region
            regionDomainModel = await regionRepository.UpdateAsync(id, regionDomainModel);

            // Check if Region exists
            if (regionDomainModel == null)
                return NotFound();

            // Map Domain model back to DTO
            //var regionDto = new RegionDto
            //{
            //    Id = regionDomainModel.Id,
            //    Name = regionDomainModel.Name,
            //    Code = regionDomainModel.Code,
            //    RegionImageUrl = regionDomainModel.RegionImageUrl,
            //};

            var regionDto = mapper.Map<RegionDto>(regionDomainModel);

            // Return DTO
            return Ok(regionDto);
        }

        // DELETE SINGLE REGION (Remove Region by ID)
        // PUT: https://localhost:44375/api/regions/{id}
        [HttpDelete]
        [Route("{id:Guid}")]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            // Use Domain Model to remove Region
            var regionDomainModel = await regionRepository.DeleteAsync(id);

            // Check if Region exists
            if (regionDomainModel == null)
                return NotFound();

            // Return DTO
            //var regionDto = new RegionDto
            //{
            //    Id = regionDomainModel.Id,
            //    Name = regionDomainModel.Name,
            //    Code = regionDomainModel.Code,
            //    RegionImageUrl = regionDomainModel.RegionImageUrl,
            //};

            var regionDto = mapper.Map<RegionDto>(regionDomainModel);

            return Ok(regionDto);
        }
    }
}
