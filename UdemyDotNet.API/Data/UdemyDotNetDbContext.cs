﻿using Microsoft.EntityFrameworkCore;
using UdemyDotNet.API.Models.Domain;

namespace UdemyDotNet.API.Data
{
    public class UdemyDotNetDbContext : DbContext
    {
        public UdemyDotNetDbContext(DbContextOptions dbContextOptions): base(dbContextOptions) { }

        public DbSet<Difficulty> Difficulties { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Walk> Walks { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Seed data for Difficulties
            // Easy, Medium, Hard
            var difficulties = new List<Difficulty>()
            {
                new Difficulty()
                {
                    Id = Guid.Parse("23344aa8-dbc8-4aa9-b694-d4d8051072a6"),
                    Name = "Easy",
                },
                new Difficulty()
                {
                    Id = Guid.Parse("0b9c7cda-d372-4d8d-b4e9-d38cfd116bb0"),
                    Name = "Medium",
                },
                new Difficulty()
                {
                    Id = Guid.Parse("dcfa98aa-ff83-4b5f-839c-0d99d0e44829"),
                    Name = "Hard",
                },
            };

            // Seed difficulties to database
            modelBuilder.Entity<Difficulty>().HasData(difficulties);


            // Seed data for Regions
            var regions = new List<Region>()
            {
                new Region()
                {
                    Id = Guid.Parse("86ed04e8-8bf1-4d75-be13-6bfe0905b5ab"),
                    Name = "Auckland",
                    Code = "AKL",
                    RegionImageUrl = "imageurl.jpg",
                },
                new Region()
                {
                    Id = Guid.Parse("ff4154e2-7ad7-4df4-89bc-024560697f3e"),
                    Name = "Wellington",
                    Code = "WGN",
                    RegionImageUrl = "imageurl.jpg",
                },
                new Region()
                {
                    Id = Guid.Parse("49dcbb62-3789-49d6-91bd-09ea646c8f5b"),
                    Name = "Nelson",
                    Code = "NSN",
                    RegionImageUrl = "imageurl.jpg",
                },
                new Region()
                {
                    Id = Guid.Parse("9af4b6ac-afb8-4ff1-acd0-2f6fadd1ca84"),
                    Name = "Southland",
                    Code = "STL",
                    RegionImageUrl = "imageurl.jpg",
                },
            };

            // Seed Regions to database
            modelBuilder.Entity<Region>().HasData(regions);        }
    }
}
