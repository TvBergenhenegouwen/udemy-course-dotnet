﻿using AutoMapper;
using UdemyDotNet.API.Models.Domain;
using UdemyDotNet.API.Models.DTO;

namespace UdemyDotNet.API.Mappings
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Region, RegionDto>().ReverseMap();

            CreateMap<Region, AddRegionDto>().ReverseMap();

            CreateMap<Region, UpdateRegionDto>().ReverseMap();

            CreateMap<Walk, WalkDto>().ReverseMap();

            CreateMap<Walk, AddWalkDto>().ReverseMap();

            CreateMap<Walk, UpdateWalkDto>().ReverseMap();

            CreateMap<Difficulty, DifficultyDto>().ReverseMap();

        }
    }
}
