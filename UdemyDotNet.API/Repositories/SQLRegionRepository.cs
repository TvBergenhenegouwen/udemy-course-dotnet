﻿using Microsoft.EntityFrameworkCore;
using UdemyDotNet.API.Data;
using UdemyDotNet.API.Models.Domain;

namespace UdemyDotNet.API.Repositories
{
    public class SQLRegionRepository : IRegionRepository
    {
        private UdemyDotNetDbContext dbContext;

        public SQLRegionRepository(UdemyDotNetDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<List<Region>> GetAllAsync()
        {
            return await dbContext.Regions.ToListAsync();
        }

        public async Task<Region?> GetByIdAsync(Guid id)
        {
            return await dbContext.Regions.FirstOrDefaultAsync(r => r.Id == id);
        }

        public async Task<Region> CreateAsync(Region region)
        {
            await dbContext.Regions.AddAsync(region);
            await dbContext.SaveChangesAsync();
            return region;
        }

        public async Task<Region?> UpdateAsync(Guid id, Region region)
        {
            var existingRegion = await dbContext.Regions.FirstOrDefaultAsync(r => r.Id == id);

            if (existingRegion == null)
                return null;

            existingRegion.Name = region.Name;
            existingRegion.Code = region.Code;
            existingRegion.RegionImageUrl = region.RegionImageUrl;

            await dbContext.SaveChangesAsync();
            return existingRegion;
        }

        public async Task<Region?> DeleteAsync(Guid id)
        {
            var regionDomainModel = await dbContext.Regions.FirstOrDefaultAsync(x => x.Id == id);
            if (regionDomainModel == null)
                return null;

            // Use Domain Model to remove Region
            dbContext.Remove(regionDomainModel);
            await dbContext.SaveChangesAsync();

            return regionDomainModel;
        }
    }
}
