﻿using UdemyDotNet.API.Models.Domain;

namespace UdemyDotNet.API.Repositories
{
    //public class InMemoryRegionRepository : IRegionRepository
    public class InMemoryRegionRepository
    {
        public List<Region> GetAllAsync()
        {
            return new List<Region>
            {
                new Region()
                {
                    Id = Guid.NewGuid(),
                    Name = "TON",
                    Code = "Toni's Region Name",
                    RegionImageUrl = "toni-regon-image-url.jpg",
                }
            };
        }
    }
}
