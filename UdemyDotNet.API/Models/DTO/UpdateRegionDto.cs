﻿using System.ComponentModel.DataAnnotations;

namespace UdemyDotNet.API.Models.DTO
{
    public class UpdateRegionDto
    {
        [Required]
        [MinLength(1, ErrorMessage = "Code has a minimum of 3 characters")]
        [MaxLength(3, ErrorMessage = "Code has a maximum of 3 characters")]
        public string Code { get; set; }

        [Required]
        [MaxLength(100, ErrorMessage = "Name has a maximum of 100 characters")]
        public string Name { get; set; }

        public string? RegionImageUrl { get; set; }
    }
}
